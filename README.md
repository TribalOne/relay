# Relay Control Class for Electronics-Salon RPi Power Relay Board expansion Module. 

## This can be used with Individual relays as well if you keep the control pins the same or change them to suit. You could also expand this class to add more relays if you wish. 

## To use this class to turn on or off a relay
## Import the class into your project by import Relays 

## Relay 1 = R1
## Relay 2 = R2
## Relay 3 = R3

### R = Relays.Relay('ON')
### R.R1() or any relay you wish to turn on. 

### R = Relays.Relay('OFF')
### R.R1() or any other realy you wish to turn off.

