#!/usr/bin/env python3
import RPi.GPIO as GPIO

# Copyright <2019> <Tribal_Pheonix>

# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# For the Electronics-Salon Rpi Power Relay Board Expansion Module for the Pi A+, B+, 2B, 3B. will also work for the RPI 4. 
# Could be used with individual Relays change pin numbers to suit or use the same pin numbers could expend to add more relays! 

# I/O Pins: 
# Relay 1 = pin 26 
# Relay 2 = pin 20
# Realy 3 = pin 21

#R = Relays.Relay('on')
#R.R1()


class Relay: 




       def __init__(self, RSwitch):
           self.RSwitch = RSwitch.lower()
           GPIO.setmode(GPIO.BCM)
           GPIO.setwarnings(False)
           GPIO.setup(26, GPIO.OUT)
           GPIO.setup(20, GPIO.OUT)
           GPIO.setup(21, GPIO.OUT)




       def R1(self):
           try: 
              if self.RSwitch == "on": 
                 #print("Relay 1 ON")
                 GPIO.output(26, GPIO.HIGH)
              elif self.RSwitch == "off":
                   #print("Relay 1 OFF")
                   GPIO.output(26, GPIO.LOW)
           except:
                 print("Failed to turn on or off Relay 1 ")




       def R2(self):
            try:
               if self.RSwitch == "on": 
                  #print("Relay 2 ON")
                  GPIO.output(20, GPIO.HIGH)
               elif self.RSwitch == "off":
                    #print("Relay 2 OFF")
                    GPIO.output(20, GPIO.LOW)
            except: 
                  print("Failed to turn on or off Relay 2")




       def R3(self): 
            try:
               if self.RSwitch == "on": 
                  #print("Relay 3 ON")
                  GPIO.output(21, GPIO.HIGH)
               elif self.RSwitch == "off":
                   #print("Relay 3 OFF")
                    GPIO.output(21, GPIO.LOW)
            except: 
                  print("Failed to turn on or off Relay 3")




if __name__ == '__main__': 
   # Test Relay Board Via running ./Relays
   print("Test Relay Board")
   import time
   R = Relay('ON')
   R.R1()
   time.sleep(2)
   R.R2()
   time.sleep(2)
   R.R3()
   time.sleep(5)
   R = Relay('off')
   R.R1()
   time.sleep(2)
   R.R2()
   time.sleep(2)
   R.R3()
   